package poc.generate_xsd_from_jaxb.jaxb.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;

public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {
    @Override
    public LocalDateTime unmarshal(String v) {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    public String marshal(LocalDateTime v) {
        throw new UnsupportedOperationException("not yet implemented");
    }
}
