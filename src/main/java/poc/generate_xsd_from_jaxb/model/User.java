package poc.generate_xsd_from_jaxb.model;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@XmlRootElement(name = "user")
@XmlType(namespace = "http://contineo/some-module/user", propOrder = {"name", "sex", "age", "createdDateTime", "friends"})
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private Integer age;

    @XmlElement(required = true)
    private LocalDateTime createdDateTime;

    @XmlAttribute
    private Long id;

    @XmlElement(required = true)
    private Sex sex;

    @XmlElementWrapper(name = "friends")
    @XmlElement(name = "friend")
    private Set<User> friends;

    @XmlType(name = "sex", namespace = "http://contineo/some-module/user")
    @XmlEnum
    public enum Sex {
        M, F
    }
}
