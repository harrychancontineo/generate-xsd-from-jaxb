### Prerequisite
- Java 8

### Generate XSD
`./mvnw clean package jaxb2:schemagen`

Then, you could find the XSD is generated in target/generated-resources/schemagen
Or you could check the XSD under [generated-xsd](generated-xsd) which is copied from the build☺

And we shall only need to pass the [user.xsd](generated-xsd/user.xsd) to client as the schema2.xsd is just storing the adaptor which would be used interanlly